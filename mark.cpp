#include "mark.h"

Mark::Mark()
{
    this->x = 0;
    this->y = 0;
    setRect(x,y,12,12);
}

Mark::Mark(int x, int y)
{
    this->x = x;
    this->y = y;
    this->state = 0;
    setRect(x,y,12,12);
    setBrush(* new QBrush(Qt::green));
    qDebug() << "Mark created at: " << x << ", " << y << endl;
}

void Mark::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if(state == 0)
    {
        setBrush(* new QBrush(Qt::red));
        state = 1;
    }
    else
    {
        setBrush(* new QBrush(Qt::green));
        state = 0;
    }
}

void Mark::clear()
{
    this->state = 0;
    setBrush(* new QBrush(Qt::green));
}
