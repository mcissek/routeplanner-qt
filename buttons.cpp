#include "buttons.h"

Button::Button(int func, Cities * cit)
{
    this->function = func;
    this->cities = cit;
    switch(func)
    {
    case 1: //start
        setPixmap(QPixmap("c:\\start.png"));

        setPos(50,450);

        break;
    case 2: //clear
        setPixmap(QPixmap("c:\\clear.png"));

        setPos(120,450);

        break;
    }
}

void Button::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    switch(this->function)
    {
    case 1:
        qDebug() << "START";
        //put your algorithm / function here
        this->cities->drawRoute();
        break;
    case 2:
        qDebug() << "CLEAR";
        this->cities->clear();
        break;
    }
}
