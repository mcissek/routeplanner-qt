#ifndef BUTTONS
#define BUTTONS

#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

#include "cities.h"

class Button : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    int function;
    Cities * cities;
public:
    Button(int func, Cities * cities);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // BUTTONS

