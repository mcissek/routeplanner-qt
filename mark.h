#ifndef MARK
#define MARK

#include <QGraphicsRectItem>
#include <QBrush>
#include <QGraphicsSceneMouseEvent>
#include <QDebug>

class Mark : public QObject, public QGraphicsRectItem
{
    Q_OBJECT
    int x;
    int y;
    int state; //1 - chosen, 0 - not
public:
    Mark();
    Mark(int x, int y);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void clear(void);
};


#endif // MARK

