#include "cities.h"

Cities::Cities(QGraphicsScene * scene)
{
    for(int i = 0; i<20; i++)
        route[i] = -1;

    route[0] = 19;
    route[1] = 0;
    route[2] = 10;
    route[3] = 15;
    route[4] = 7;
    route[5] = 2;

    this->scene = scene;
    for (int i=0; i<20; i++)
    {
        cities_tab[i] = new Mark(cities_h[i], cities_w[i]);
        if(i == 19)
        {
            cities_tab[i]->setBrush(Qt::blue);
            cities_tab[i]->setEnabled(false);
        }
        scene->addItem(cities_tab[i]);
    }

    scene->update();
}

void Cities::drawRoute()
{
    int i=1;
    const int offset = 6;
    while (route[i] != -1)
    {
        int ax = cities_h[route[i-1]] + offset;
        int ay = cities_w[route[i-1]] + offset;

        int bx = cities_h[route[i]] + offset;
        int by = cities_w[route[i]] + offset;

        QGraphicsLineItem * tmp = new QGraphicsLineItem (ax,ay,bx,by);
        this->lines_list.push_back(tmp);
        this->scene->addItem(tmp);

        this->scene->update();

        i++;
    }
}

void Cities::clear()
{
    while(!lines_list.isEmpty())
    {
        this->scene->removeItem(lines_list.back());
        lines_list.pop_back();
    }
    this->scene->update();

    for (int i=0; i<19; i++)
    {
        cities_tab[i]->clear();
    }
}
