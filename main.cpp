#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QCursor>
#include <QDebug>

#include "cities.h"
#include "buttons.h"

/*TO DO:
- get PL map and diplay it on scene,
- add items to mark main cities,
- add click listeners to determine which items was clicked,
- add container to hold data about choosen cities,
*/

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QGraphicsScene * scene = new QGraphicsScene();
    QGraphicsView * view = new QGraphicsView(scene);
    QGraphicsPixmapItem item(QPixmap("c:\\map1.png"));
    scene->addItem(&item);

    Cities * cit = new Cities(scene);
    Button * but_start = new Button(1, cit);
    Button * but_clear = new Button(2, cit);

    scene->addItem(but_start);
    scene->addItem(but_clear);
    scene->update();

    view->show();
    return a.exec();
}
