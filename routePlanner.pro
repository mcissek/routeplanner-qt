#-------------------------------------------------
#
# Project created by QtCreator 2016-05-26T16:25:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = routePlanner
TEMPLATE = app


SOURCES += main.cpp \
    mark.cpp \
    cities.cpp \
    buttons.cpp

HEADERS  += \
    mark.h \
    cities.h \
    buttons.h
